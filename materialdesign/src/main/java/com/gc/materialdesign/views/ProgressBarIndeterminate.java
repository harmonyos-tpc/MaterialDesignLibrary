package com.gc.materialdesign.views;

import com.gc.materialdesign.utils.ViewHelper;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.app.Context;

public class ProgressBarIndeterminate extends ProgressBarDeterminate {
    public ProgressBarIndeterminate(Context context) {
        this(context, null);
    }

    public ProgressBarIndeterminate(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public ProgressBarIndeterminate(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        super.onDraw(component, canvas);
        setProgress(60);
        AnimatorProperty animator = new AnimatorProperty(progressView);
        animator.scaleXFrom(0.5f).scaleX(2);
        animator.scaleYFrom(1).scaleY(1);
        animator.setDuration(1000);
        animator.setLoopedCount(AnimatorProperty.INFINITE);
        animator.start();

        AnimatorProperty animatorProperty = new AnimatorProperty(progressView);
        animatorProperty.moveFromX(0).moveToX(getWidth());
        animatorProperty.setDuration(1200);
        animatorProperty.setStateChangedListener(listener);
        animatorProperty.start();
    }

    private Animator.StateChangedListener listener =
            new Animator.StateChangedListener() {
                int cont = 1;
                int suma = 1;
                int duration = 1200;

                @Override
                public void onStart(Animator animator) {}

                @Override
                public void onStop(Animator animator) {}

                @Override
                public void onCancel(Animator animator) {}

                @Override
                public void onEnd(Animator animator) {
                    // Repeat animation
                    ViewHelper.setX(progressView, -progressView.getWidth() / 2);
                    cont += suma;
                    try {
                        AnimatorProperty animatorRepeat = new AnimatorProperty(progressView);
                        animatorRepeat.moveFromX(0).moveToX(getWidth());
                        animatorRepeat.setDuration(duration / cont);
                        animatorRepeat.setStateChangedListener(this);
                        animatorRepeat.start();
                    } catch (ArrayIndexOutOfBoundsException e) {
                        e.toString();
                        // ignore this error that sometimes comes from the NineOldAndroids 2.4 library
                    }
                    if (cont == 3 || cont == 1) {
                        suma *= -1;
                    }
                }

                @Override
                public void onPause(Animator animator) {}

                @Override
                public void onResume(Animator animator) {}
            };
}
