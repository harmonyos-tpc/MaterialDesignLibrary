package com.gc.materialdesign.views;

import com.gc.materialdesign.ResourceTable;
import com.gc.materialdesign.utils.ShapeUtil;
import com.gc.materialdesign.utils.Utils;
import com.gc.materialdesign.utils.ViewHelper;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

public class ButtonFloat extends Button {
    int sizeIcon = 24;
    int sizeRadius;

    private Image icon; // Icon of float button
    private Element drawableIcon;

    private boolean isShow = false;

    private float showPosition;
    private float hidePosition;
    private boolean animate;

    public ButtonFloat(Context context) {
        this(context, null);
    }

    public ButtonFloat(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public ButtonFloat(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        sizeRadius = 28;
        setDefaultProperties();
        icon = new Image(context);
        icon.setScaleMode(Image.ScaleMode.CLIP_CENTER);

        if (drawableIcon != null) {
            icon.setImageElement(drawableIcon);
        }
        LayoutConfig layoutConfig =
                new DependentLayout.LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        layoutConfig.addRule(LayoutConfig.CENTER_IN_PARENT, LayoutConfig.TRUE);
        icon.setLayoutConfig(layoutConfig);
        addComponent(icon);
    }

    protected void setDefaultProperties() {
        rippleSpeed = Utils.dpToPx(2, context);
        rippleSize = Utils.dpToPx(5, context);
        setMinHeight(Utils.dpToPx(sizeRadius * 2, context));
        setMinWidth(Utils.dpToPx(sizeRadius * 2, context));
        super.background = ResourceTable.Graphic_background_button_float;
    }

    // Set atributtes of XML to View
    protected void setAttributes(AttrSet attrs) {
        // Set background Color
        // Color by resource
        boolean backgroundIsPresent = attrs.getAttr("background").isPresent();
        if (backgroundIsPresent) {
            String color = attrs.getAttr("background").get().getStringValue();
            setBackgroundColor(color);
            setBackground(ShapeUtil.createCircleDrawable(Color.getIntColor(Utils.rgba(color))));
        } else {
            ShapeElement shapeElement = new ShapeElement(context, ResourceTable.Graphic_background_button_float);
            setBackground(shapeElement);
            setBackgroundColor(backgroundColor);
        }

        // Set Ripple Color
        // Color by resource
        boolean rippleColorIsPresent = attrs.getAttr("rippleColor").isPresent();
        if (rippleColorIsPresent) {
            String rippleColor = attrs.getAttr("rippleColor").get().getStringValue();
            setRippleColor(rippleColor);
        } else {
            setRippleColor(makePressColorString());
        }
        // Icon of button
        boolean iconDrawableIsPresent = attrs.getAttr("iconDrawable").isPresent();
        if (iconDrawableIsPresent) {
            Element element = attrs.getAttr("iconDrawable").get().getElement();
            drawableIcon = element;
        }
        boolean animateIsPresent = attrs.getAttr("animate").isPresent();
        if (animateIsPresent) {
            animate = attrs.getAttr("animate").get().getBoolValue();
        }

        getComponentTreeObserver().addWindowBoundListener(new ComponentTreeObserver.WindowBoundListener() {
            @Override
            public void onWindowBound() {
                showPosition = ViewHelper.getY(ButtonFloat.this) - Utils.dpToPx(24, context);
                hidePosition = ViewHelper.getY(ButtonFloat.this) + getHeight() * 3;
                if (animate) {
                    ViewHelper.setY(ButtonFloat.this, hidePosition);
                    show();
                }
            }

            @Override
            public void onWindowUnbound() {

            }
        });
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        super.onDraw(component, canvas);
        if (x != -1) {
            RectFloat src = new RectFloat(0, 0, getWidth(), getHeight());
            RectFloat dst =
                    new RectFloat(
                            Utils.dpToPx(1, context),
                            Utils.dpToPx(2, context),
                            getWidth() - Utils.dpToPx(1, context),
                            getHeight() - Utils.dpToPx(2, context));
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            canvas.drawPixelMapHolderRect(new PixelMapHolder(cropCircle(makeCircle())), src, dst, paint);
            getContext().getUITaskDispatcher().asyncDispatch(this::invalidate);
        }
    }

    public Image getIcon() {
        return icon;
    }

    public void setIcon(Image icon) {
        this.icon = icon;
    }

    public Element getDrawableIcon() {
        return drawableIcon;
    }

    public void setDrawableIcon(Element drawableIcon) {
        this.drawableIcon = drawableIcon;
        try {
            icon.setImageElement(drawableIcon);
        } catch (NoSuchMethodError e) {
            e.toString();
        }
    }

    private PixelMap cropCircle(PixelMap bitmap) {
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
        Size srcSize = bitmap.getImageInfo().size;
        initializationOptions.size = new Size(srcSize.width, srcSize.height);
        PixelMap pixelMap = PixelMap.create(initializationOptions);

        Texture texture = new Texture(pixelMap);
        Canvas canvas = new Canvas(texture);

        final int color = 0XFFFF4242;
        final Paint paint = new Paint();
        final RectFloat rectFloat = new RectFloat(0, 0, srcSize.width, srcSize.height);

        paint.setAntiAlias(true);
        paint.setColor(new Color(color));
        canvas.drawCircle(srcSize.width / 2, srcSize.height / 2, srcSize.width / 2, paint);
        paint.setBlendMode(BlendMode.SRC_IN);
        canvas.drawPixelMapHolderRect(new PixelMapHolder(bitmap), rectFloat, rectFloat, paint);
        return texture.getPixelMap();
    }

    @Override
    public Text getTextView() {
        return null;
    }

    public void setRippleColor(String rippleColor) {
        this.rippleColor = rippleColor;
    }

    public void show() {
        AnimatorProperty animator = createAnimatorProperty();
        animator.moveFromY(getContentPositionY()).moveToY(showPosition);
        animator.setCurveType(Animator.CurveType.BOUNCE);
        animator.setDuration(1500);
        animator.start();
        isShow = true;
    }

    public void hide() {
        AnimatorProperty animator = createAnimatorProperty();
        animator.moveFromY(getContentPositionY()).moveToY(hidePosition);
        animator.setCurveType(Animator.CurveType.BOUNCE);
        animator.setDuration(1500);
        animator.start();
        isShow = false;
    }

    public boolean isShow() {
        return isShow;
    }

    // Set color of background
    public void setBackgroundColor(String color) {
        super.setBackgroundColor(color);
        setBackground(ShapeUtil.createCircleDrawable(Color.getIntColor(Utils.rgba(color))));
    }
}
