package com.gc.materialdesign.views;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

public class ScrollView extends ohos.agp.components.ScrollView implements Component.TouchEventListener {
    /*
     * This class avoid problems in scrollviews with elements in library
     * Use it if you want use a ScrollView in your App
     */

    public ScrollView(Context context) {
        this(context, null);
    }

    public ScrollView(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public ScrollView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTouchEventListener(this);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        for (int i = 0; i < getChildCount(); i++) {
            try {
                CustomView child = (CustomView) getComponentAt(i);
                if (child.isLastTouch) {
                    return true;
                }
            } catch (ClassCastException e) {
                e.toString();
            }
        }
        return true;
    }
}
