package com.gc.materialdesign.widgets;

import com.gc.materialdesign.ResourceTable;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;

public class Dialog {
    private Context context;
    private Component view;
    private Component backView;
    private String message;
    private Text messageTextView;
    private String title;
    private Text titleTextView;

    private Button buttonAccept;
    private Button buttonCancel;

    private String buttonCancelText;

    private Component.ClickedListener onAcceptButtonClickListener;
    private Component.ClickedListener onCancelButtonClickListener;
    private CommonDialog dialog;

    public Dialog(Context context, String title, String message) {
        this.context = context; // init Context
        this.message = message;
        this.title = title;
        init();
    }

    public void addCancelButton(String buttonCancelText) {
        this.buttonCancelText = buttonCancelText;
    }

    public void addCancelButton(String buttonCancelText, Component.ClickedListener onCancelButtonClickListener) {
        this.buttonCancelText = buttonCancelText;
        this.onCancelButtonClickListener = onCancelButtonClickListener;
    }

    private void init() {
        dialog = new CommonDialog(context);
        dialog.setTransparent(true);
        Component component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_dialog, null, false);
        dialog.setContentCustomComponent(component);

        view = component.findComponentById(ResourceTable.Id_contentDialog);
        backView = component.findComponentById(ResourceTable.Id_dialog_rootView);
        backView.setTouchEventListener(
                (touchComponent, event) -> {
                    MmiPoint mmiPoint = event.getPointerScreenPosition(event.getIndex());
                    float x = Math.abs(mmiPoint.getX() - touchComponent.getLocationOnScreen()[0]);
                    float y = Math.abs(mmiPoint.getY() - touchComponent.getLocationOnScreen()[1]);
                    if (x < view.getLeft() || x > view.getRight() || y > view.getBottom() || y < view.getTop()) {
                        dismiss();
                    }
                    return false;
                });

        titleTextView = (Text) component.findComponentById(ResourceTable.Id_title);
        setTitle(title);

        messageTextView = (Text) component.findComponentById(ResourceTable.Id_message);
        setMessage(message);

        buttonAccept = (Button) component.findComponentById(ResourceTable.Id_button_accept);
        buttonAccept.setClickedListener(
                view -> {
                    dismiss();
                    if (onAcceptButtonClickListener != null) {
                        onAcceptButtonClickListener.onClick(view);
                    }
                });

        if (buttonCancelText != null) {
            buttonCancel = (Button) component.findComponentById(ResourceTable.Id_button_cancel);
            buttonCancel.setVisibility(Component.VISIBLE);
            buttonCancel.setText(buttonCancelText.toUpperCase());
            buttonCancel.setClickedListener(
                    view -> {
                        dismiss();
                        if (onCancelButtonClickListener != null) {
                            onCancelButtonClickListener.onClick(view);
                        }
                    });
        }
    }

    public void show() {
        dialog.show();
        // set dialog enter animations
        view.setAlpha(0);
        backView.setAlpha(0);
        view.createAnimatorProperty().alpha(1).setDuration(200).start();
        backView.createAnimatorProperty().alpha(1).setDuration(200).start();
    }

    // GETERS & SETTERS

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
        messageTextView.setText(message);
    }

    public Text getMessageTextView() {
        return messageTextView;
    }

    public void setMessageTextView(Text messageTextView) {
        this.messageTextView = messageTextView;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        if (title == null) {
            titleTextView.setVisibility(Component.HIDE);
        } else {
            titleTextView.setVisibility(Component.VISIBLE);
            titleTextView.setText(title);
        }
    }

    public Text getTitleTextView() {
        return titleTextView;
    }

    public void setTitleTextView(Text titleTextView) {
        this.titleTextView = titleTextView;
    }

    public Button getButtonAccept() {
        return buttonAccept;
    }

    public void setButtonAccept(Button buttonAccept) {
        this.buttonAccept = buttonAccept;
    }

    public Button getButtonCancel() {
        return buttonCancel;
    }

    public void setButtonCancel(Button buttonCancel) {
        this.buttonCancel = buttonCancel;
    }

    public void setOnAcceptButtonClickListener(Component.ClickedListener onAcceptButtonClickListener) {
        this.onAcceptButtonClickListener = onAcceptButtonClickListener;
        if (buttonAccept != null) {
            buttonAccept.setClickedListener(onAcceptButtonClickListener);
        }
    }

    public void setOnCancelButtonClickListener(Component.ClickedListener onCancelButtonClickListener) {
        this.onCancelButtonClickListener = onCancelButtonClickListener;
        if (buttonCancel != null) {
            buttonCancel.setClickedListener(onCancelButtonClickListener);
        }
    }

    public void dismiss() {
        view.setAlpha(1);
        AnimatorProperty anim = new AnimatorProperty(view);
        anim.alpha(0).scaleXBy(0.8f).scaleYBy(0.8f);
        anim.setDelay(50);
        anim.setDuration(200);
        anim.setCurveType(Animator.CurveType.ACCELERATE);
        anim.setStateChangedListener(
                new Animator.StateChangedListener() {
                    @Override
                    public void onStart(Animator animator) {}

                    @Override
                    public void onStop(Animator animator) {}

                    @Override
                    public void onCancel(Animator animator) {}

                    @Override
                    public void onEnd(Animator animator) {
                        dialog.hide();
                    }

                    @Override
                    public void onPause(Animator animator) {}

                    @Override
                    public void onResume(Animator animator) {}
                });

        backView.setAlpha(1);
        AnimatorProperty backAnim = new AnimatorProperty(backView);
        backAnim.alpha(0);
        backAnim.setDelay(50);
        backAnim.setDuration(200);
        anim.setCurveType(Animator.CurveType.ACCELERATE);

        anim.start();
        backAnim.start();
    }
}
