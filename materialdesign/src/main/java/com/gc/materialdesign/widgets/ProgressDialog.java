package com.gc.materialdesign.widgets;

import com.gc.materialdesign.ResourceTable;
import com.gc.materialdesign.utils.StringUtil;
import com.gc.materialdesign.views.ProgressBarCircularIndeterminate;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.multimodalinput.event.MmiPoint;

public class ProgressDialog {
    private Context context;
    private Component view;
    private Component backView;
    private String title;
    private Text titleTextView;

    private String progressColor;
    private CommonDialog dialog;
    private EventHandler handler;

    public ProgressDialog(Context context, String title) {
        this.title = title;
        this.context = context;
        init();
    }

    public ProgressDialog(Context context, String title, String progressColor) {
        this.title = title;
        this.progressColor = progressColor;
        this.context = context;
        init();
    }

    private void init() {
        handler = new EventHandler(EventRunner.getMainEventRunner());
        dialog = new CommonDialog(context);
        dialog.setTransparent(true);
        Component component =
                LayoutScatter.getInstance(context).parse(ResourceTable.Layout_progress_dialog, null, false);
        dialog.setContentCustomComponent(component);

        view = component.findComponentById(ResourceTable.Id_contentDialog);
        backView = component.findComponentById(ResourceTable.Id_dialog_rootView);
        backView.setTouchEventListener(
                (touchComponent, event) -> {
                    MmiPoint mmiPoint = event.getPointerScreenPosition(event.getIndex());
                    float x = Math.abs(mmiPoint.getX() - touchComponent.getLocationOnScreen()[0]);
                    float y = Math.abs(mmiPoint.getY() - touchComponent.getLocationOnScreen()[1]);
                    if (x < view.getLeft() || x > view.getRight() || y > view.getBottom() || y < view.getTop()) {
                        dialog.hide();
                    }
                    return false;
                });

        titleTextView = (Text) component.findComponentById(ResourceTable.Id_title);
        setTitle(title);
        if (!StringUtil.isEmpty(progressColor)) {
            ProgressBarCircularIndeterminate progressBarCircularIndeterminate =
                    (ProgressBarCircularIndeterminate)
                            component.findComponentById(ResourceTable.Id_progressBarCircularIndetermininate);
            progressBarCircularIndeterminate.setBackgroundColor(progressColor);
        }
    }

    public void show() {
        handler.postTask(
                () -> {
                    dialog.show();
                    // set dialog enter animations
                    view.setAlpha(0);
                    backView.setAlpha(0);
                    view.createAnimatorProperty().alpha(1).setDuration(200).start();
                    backView.createAnimatorProperty().alpha(1).setDuration(200).start();
                });
    }

    // GETERS & SETTERS

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        if (title == null) {
            titleTextView.setVisibility(Component.HIDE);
        } else {
            titleTextView.setVisibility(Component.VISIBLE);
            titleTextView.setText(title);
        }
    }

    public Text getTitleTextView() {
        return titleTextView;
    }

    public void setTitleTextView(Text titleTextView) {
        this.titleTextView = titleTextView;
    }

    public void hide() {
        view.setAlpha(1);
        AnimatorProperty anim = new AnimatorProperty(view);
        anim.alpha(0).scaleXBy(0.8f).scaleYBy(0.8f);
        anim.setDelay(50);
        anim.setDuration(200);
        anim.setCurveType(Animator.CurveType.DECELERATE);
        anim.setStateChangedListener(
                new Animator.StateChangedListener() {
                    @Override
                    public void onStart(Animator animator) {}

                    @Override
                    public void onStop(Animator animator) {}

                    @Override
                    public void onCancel(Animator animator) {}

                    @Override
                    public void onEnd(Animator animator) {
                        handler.postTask(
                                () -> {
                                    dialog.hide();
                                });
                    }

                    @Override
                    public void onPause(Animator animator) {}

                    @Override
                    public void onResume(Animator animator) {}
                });

        backView.setAlpha(1);
        AnimatorProperty backAnim = new AnimatorProperty(backView);
        backAnim.alpha(0);
        backAnim.setDelay(50);
        backAnim.setDuration(200);
        anim.setCurveType(Animator.CurveType.DECELERATE);

        anim.start();
        backAnim.start();
    }
}
