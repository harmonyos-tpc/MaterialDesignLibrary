package com.gc.materialdesign.widgets;

import com.gc.materialdesign.ResourceTable;
import com.gc.materialdesign.utils.ShapeUtil;
import com.gc.materialdesign.utils.Utils;
import com.gc.materialdesign.views.Slider;
import com.gc.materialdesign.views.Slider.OnValueChangedListener;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;

public class ColorSelector extends CommonDialog implements OnValueChangedListener {
    private String color = "#000000";
    private Context context;
    private Component colorView;
    private Component view, backView; // background

    private OnColorSelectedListener onColorSelectedListener;
    private Slider red, green, blue;

    public ColorSelector(Context context, String color, OnColorSelectedListener onColorSelectedListener) {
        super(context);
        this.context = context;
        this.onColorSelectedListener = onColorSelectedListener;
        if (color != null) {
            this.color = color;
        }
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        setTransparent(true);
        Component component =
                LayoutScatter.getInstance(context).parse(ResourceTable.Layout_color_selector, null, false);
        setContentCustomComponent(component);

        view = component.findComponentById(ResourceTable.Id_contentSelector);
        backView = component.findComponentById(ResourceTable.Id_rootSelector);
        backView.setTouchEventListener(
                (touchComponent, event) -> {
                    MmiPoint mmiPoint = event.getPointerScreenPosition(event.getIndex());
                    float x = Math.abs(mmiPoint.getX() - touchComponent.getLocationOnScreen()[0]);
                    float y = Math.abs(mmiPoint.getY() - touchComponent.getLocationOnScreen()[1]);
                    if (x < view.getLeft() || x > view.getRight() || y > view.getBottom() || y < view.getTop()) {
                        hide();
                    }
                    return false;
                });

        colorView = component.findComponentById(ResourceTable.Id_viewColor);
        colorView.setBackground(ShapeUtil.createDrawable(Color.getIntColor(Utils.rgba(color))));
        // Resize ColorView

        // Configure Sliders
        red = (Slider) component.findComponentById(ResourceTable.Id_red);
        green = (Slider) component.findComponentById(ResourceTable.Id_green);
        blue = (Slider) component.findComponentById(ResourceTable.Id_blue);

        int colorInt;
        if (color.length() == 7) {
            colorInt = Color.getIntColor(color);
        } else {
            colorInt = Color.getIntColor(color.substring(0, color.length() - 2));
        }
        int r = (colorInt >> 16) & 0xFF;
        int g = (colorInt >> 8) & 0xFF;
        int b = (colorInt >> 0) & 0xFF;

        red.setValue(r);
        green.setValue(g);
        blue.setValue(b);

        red.setOnValueChangedListener(this);
        green.setOnValueChangedListener(this);
        blue.setOnValueChangedListener(this);
    }

    @Override
    public void show() {
        super.show();
        view.setAlpha(0);
        backView.setAlpha(0);
        view.createAnimatorProperty().alpha(1).setDuration(200).start();
        backView.createAnimatorProperty().alpha(1).setDuration(200).start();
    }

    @Override
    public void onValueChanged(int value) {
        StringBuilder builder =
                new StringBuilder("#")
                        .append(colorPrefix(Integer.toHexString(red.getValue())))
                        .append(colorPrefix(Integer.toHexString(green.getValue())))
                        .append(colorPrefix(Integer.toHexString(blue.getValue())));
        color = builder.toString();
        colorView.setBackground(ShapeUtil.createDrawable(Color.getIntColor(Utils.rgba(color))));
    }

    private String colorPrefix(String string) {
        return string.length() > 1 ? string : "00";
    }

    // Event that execute when color selector is closed
    public interface OnColorSelectedListener {
        void onColorSelected(String color);
    }

    @Override
    protected void onHide() {
        view.setAlpha(1);
        AnimatorProperty anim = new AnimatorProperty(view);
        anim.alpha(0).scaleXBy(0.8f).scaleYBy(0.8f);
        anim.setDelay(50);
        anim.setDuration(200);
        anim.setCurveType(Animator.CurveType.ACCELERATE);
        anim.setStateChangedListener(
                new Animator.StateChangedListener() {
                    @Override
                    public void onStart(Animator animator) {}

                    @Override
                    public void onStop(Animator animator) {}

                    @Override
                    public void onCancel(Animator animator) {}

                    @Override
                    public void onEnd(Animator animator) {
                        hide();
                    }

                    @Override
                    public void onPause(Animator animator) {}

                    @Override
                    public void onResume(Animator animator) {}
                });

        backView.setAlpha(1);
        AnimatorProperty backAnim = new AnimatorProperty(backView);
        backAnim.alpha(0);
        backAnim.setDelay(50);
        backAnim.setDuration(200);
        anim.setCurveType(Animator.CurveType.ACCELERATE);

        anim.start();
        backAnim.start();

        if (onColorSelectedListener != null) {
            onColorSelectedListener.onColorSelected(this.color);
        }
    }
}
