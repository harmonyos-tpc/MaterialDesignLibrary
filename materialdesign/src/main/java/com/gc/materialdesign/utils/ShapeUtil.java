/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gc.materialdesign.utils;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.ShapeElement;

/**
 * shape图形工具类
 */
public class ShapeUtil {

    /**
     * 根据颜色创建shape图形
     * @param bgColor 颜色
     * @return shape图形
     */
    public static ShapeElement createDrawable(int bgColor) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(bgColor));
        return shapeElement;
    }

    /**
     * 根据颜色，角度创建shape图形
     * @param color 颜色
     * @param radius 角度
     * @return shape图形
     */
    public static ShapeElement createDrawable(int color, float radius) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(ShapeElement.RECTANGLE);
        drawable.setRgbColor(new RgbColor(color));
        drawable.setCornerRadius(radius);
        return drawable;
    }

    /**
     * 根据颜色创建圆形shape图形
     * @param color 颜色
     * @return shape图形
     */
    public static ShapeElement createCircleDrawable(int color) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(ShapeElement.OVAL);
        drawable.setRgbColor(new RgbColor(color));
        return drawable;
    }

    /**
     * 根据颜色，角度创建shape图形
     * @param color 颜色
     * @param tlRadius 左上角度
     * @param trRadius 右上角度
     * @param brRadius 右下角度
     * @param blRadius 左下角度
     * @return shape图形
     */
    public static ShapeElement createDrawable(
            int color, float tlRadius, float trRadius, float brRadius, float blRadius) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(ShapeElement.RECTANGLE);
        drawable.setRgbColor(new RgbColor(color));
        drawable.setCornerRadiiArray(
                new float[] {
                    tlRadius, tlRadius,
                    trRadius, trRadius,
                    brRadius, brRadius,
                    blRadius, blRadius
                });
        return drawable;
    }

}
