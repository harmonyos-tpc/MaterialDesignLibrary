/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gc.materialdesign.utils;

import ohos.agp.components.Component;

/**
 * 获取组件相对屏幕的数据
 */
public class ViewHelper {
    private ViewHelper() {}

    /**
     * 获取组件距离屏幕左边的距离
     * @param view 目标组件
     * @return 组件距离屏幕左边的距离
     */
    public static float getX(Component view) {
        return view.getContentPositionX();
    }

    /**
     * 设置组件距离屏幕左边的距离
     * @param view 目标组件
     * @param xValue 距离
     */
    public static void setX(Component view, float xValue) {
        view.setContentPosition(xValue, view.getContentPositionY());
    }

    /**
     * 获取组件距离屏幕上边的距离
     * @param view 目标组件
     * @return 组件距离屏幕上边的距离
     */
    public static float getY(Component view) {
        return view.getContentPositionY();
    }

    /**
     * 设置组件距离屏幕上边的距离
     * @param view 目标组件
     * @param yValue 距离
     */
    public static void setY(Component view, float yValue) {
        view.setContentPosition(view.getContentPositionX(), yValue);
    }

}
