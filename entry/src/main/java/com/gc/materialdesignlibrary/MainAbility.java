/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gc.materialdesignlibrary;

import com.gc.materialdesign.utils.ViewHelper;
import com.gc.materialdesign.views.ButtonFloatSmall;
import com.gc.materialdesign.views.LayoutRipple;
import com.gc.materialdesign.widgets.ColorSelector;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;

public class MainAbility extends Ability implements ColorSelector.OnColorSelectedListener {
    private String backgroundColor = "#1E88E5";
    private ButtonFloatSmall buttonSelectColor;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);

        buttonSelectColor = (ButtonFloatSmall) findComponentById(ResourceTable.Id_buttonColorSelector);
        buttonSelectColor.setClickedListener(
                component -> {
                    ColorSelector colorSelector =
                            new ColorSelector(MainAbility.this, backgroundColor, MainAbility.this);
                    colorSelector.show();
                });

        LayoutRipple layoutRipple = (LayoutRipple) findComponentById(ResourceTable.Id_itemButtons);
        setOriginRipple(layoutRipple);
        layoutRipple.setClickedListener(
                component -> {
                    launchAbility("com.gc.materialdesignlibrary.ButtonsAbility");
                });

        layoutRipple = (LayoutRipple) findComponentById(ResourceTable.Id_itemSwitches);
        setOriginRipple(layoutRipple);
        layoutRipple.setClickedListener(
                component -> {
                    launchAbility("com.gc.materialdesignlibrary.SwitchAbility");
                });

        layoutRipple = (LayoutRipple) findComponentById(ResourceTable.Id_itemProgress);
        setOriginRipple(layoutRipple);
        layoutRipple.setClickedListener(
                component -> {
                    launchAbility("com.gc.materialdesignlibrary.ProgressAbility");
                });

        layoutRipple = (LayoutRipple) findComponentById(ResourceTable.Id_itemWidgets);
        setOriginRipple(layoutRipple);
        layoutRipple.setClickedListener(
                component -> {
                    launchAbility("com.gc.materialdesignlibrary.WidgetAbility");
                });
    }

    private void launchAbility(String abilityName) {
        Intent intent = new Intent();
        Operation operation =
                new Intent.OperationBuilder()
                        .withBundleName("com.gc.materialdesignlibrary")
                        .withAbilityName(abilityName)
                        .build();
        intent.setOperation(operation);
        intent.setParam("BACKGROUND", backgroundColor);
        startAbility(intent);
    }

    private void setOriginRipple(LayoutRipple layoutRipple) {
        layoutRipple.setLayoutRefreshedListener(
                component -> {
                    Component v = layoutRipple.getComponentAt(0);
                    layoutRipple.setxRippleOrigin(ViewHelper.getX(v) + v.getWidth() / 2);
                    layoutRipple.setyRippleOrigin(ViewHelper.getY(v) + v.getHeight() / 2);
                    layoutRipple.setRippleColor("#1E88E5");
                    layoutRipple.setRippleSpeed(30);
                });
    }

    @Override
    public void onColorSelected(String color) {
        backgroundColor = color;
        buttonSelectColor.setBackgroundColor(color);
    }
}
