/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gc.materialdesignlibrary;

import com.gc.materialdesign.views.*;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ButtonsAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_buttons);
        String color = getIntent().getStringParam("BACKGROUND");
        ButtonFlat buttonFlat = (ButtonFlat) findComponentById(ResourceTable.Id_buttonflat);
        buttonFlat.setBackgroundColor(color);
        Button button = (Button) findComponentById(ResourceTable.Id_button);
        button.setBackgroundColor(color);
        ButtonFloatSmall buttonFloatSmall = (ButtonFloatSmall) findComponentById(ResourceTable.Id_buttonFloatSmall);
        buttonFloatSmall.setBackgroundColor(color);
        ButtonIcon buttonIcon = (ButtonIcon) findComponentById(ResourceTable.Id_buttonIcon);
        buttonIcon.setBackgroundColor(color);
        ButtonFloat buttonFloat = (ButtonFloat) findComponentById(ResourceTable.Id_buttonFloat);
        buttonFloat.setBackgroundColor(color);
    }
}
