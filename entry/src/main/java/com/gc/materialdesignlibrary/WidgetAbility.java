/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gc.materialdesignlibrary;

import com.gc.materialdesign.views.ButtonFlat;
import com.gc.materialdesign.widgets.ColorSelector;
import com.gc.materialdesign.widgets.Dialog;
import com.gc.materialdesign.widgets.SnackBar;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;

public class WidgetAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_widget);

        //	SHOW SNACKBAR
        findComponentById(ResourceTable.Id_buttonSnackBar)
                .setClickedListener(
                        new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                new SnackBar(
                                                WidgetAbility.this,
                                                "Do you want change color of this button to red?",
                                                "yes",
                                                new Component.ClickedListener() {
                                                    @Override
                                                    public void onClick(Component component) {
                                                        ButtonFlat btn =
                                                                (ButtonFlat)
                                                                        findComponentById(
                                                                                ResourceTable.Id_buttonSnackBar);
                                                        btn.setTextColor(Color.RED);
                                                    }
                                                })
                                        .show();
                            }
                        });
        //	SHOW DiALOG
        findComponentById(ResourceTable.Id_buttonDialog)
                .setClickedListener(
                        new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                Dialog dialog =
                                        new Dialog(
                                                WidgetAbility.this,
                                                "Title",
                                                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do"
                                                    + " eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut"
                                                    + " enim ad minim veniam");
                                dialog.setOnAcceptButtonClickListener(
                                        new Component.ClickedListener() {
                                            @Override
                                            public void onClick(Component component) {
                                                new ToastDialog(WidgetAbility.this)
                                                        .setText("Click accept button")
                                                        .setDuration(1000)
                                                        .show();
                                            }
                                        });
                                dialog.setOnCancelButtonClickListener(
                                        new Component.ClickedListener() {
                                            @Override
                                            public void onClick(Component component) {
                                                new ToastDialog(WidgetAbility.this)
                                                        .setText("Click cancel button")
                                                        .setDuration(1000)
                                                        .show();
                                            }
                                        });
                                dialog.show();
                            }
                        });
        // SHOW COLOR SEECTOR
        findComponentById(ResourceTable.Id_buttonColorSelector)
                .setClickedListener(
                        new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                new ColorSelector(WidgetAbility.this, "#FF0000", null).show();
                            }
                        });
    }
}
