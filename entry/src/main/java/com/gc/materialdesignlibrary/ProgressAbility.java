/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gc.materialdesignlibrary;

import com.gc.materialdesign.utils.ShapeUtil;
import com.gc.materialdesign.utils.Utils;
import com.gc.materialdesign.views.*;
import com.gc.materialdesign.widgets.ProgressDialog;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

public class ProgressAbility extends Ability {
    private ProgressBarIndeterminateDeterminate progressBarIndeterminateDeterminate;
    private ProgressBarDeterminate progressBarDeterminate;
    private TaskHandler handler;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_progress);
        String color = getIntent().getStringParam("BACKGROUND");
        ProgressBarCircularIndeterminate progressBarCircularIndeterminate =
                (ProgressBarCircularIndeterminate)
                        findComponentById(ResourceTable.Id_progressBarCircularIndetermininate);
        progressBarCircularIndeterminate.setBackgroundColor(color);

        ProgressBarIndeterminate progressBarIndeterminate =
                (ProgressBarIndeterminate) findComponentById(ResourceTable.Id_progressBarIndeterminate);
        progressBarIndeterminate.setBackgroundColor(color);

        progressBarIndeterminateDeterminate =
                (ProgressBarIndeterminateDeterminate)
                        findComponentById(ResourceTable.Id_progressBarIndeterminateDeterminate);
        progressBarIndeterminateDeterminate.setBackgroundColor(color);

        progressBarDeterminate = (ProgressBarDeterminate) findComponentById(ResourceTable.Id_progressDeterminate);
        progressBarDeterminate.setBackgroundColor(color);

        Slider slider = (Slider) findComponentById(ResourceTable.Id_slider);
        slider.setBackgroundColor(color);
        Slider sliderNumber = (Slider) findComponentById(ResourceTable.Id_sliderNumber);
        sliderNumber.setBackgroundColor(color);

        handler = new TaskHandler(EventRunner.getMainEventRunner());
        progressTimer.start();
        progressTimer2.start();

        ohos.agp.components.Button button = (ohos.agp.components.Button) findComponentById(ResourceTable.Id_btn);
        button.setBackground(ShapeUtil.createDrawable(Color.getIntColor(Utils.rgba(color))));
        findComponentById(ResourceTable.Id_btn)
                .setClickedListener(component -> new ProgressDialog(ProgressAbility.this, "Loading").show());
    }

    private Thread progressTimer =
            new Thread(
                    () -> {
                        for (int i = 0; i <= 100; i++) {
                            try {
                                Thread.sleep(100);
                                handler.sendEvent(502);
                            } catch (InterruptedException e) {

                            }
                        }
                    });

    private Thread progressTimer2 =
            new Thread(
                    () -> {
                        try {
                            Thread.sleep(4000);
                            for (int i = 0; i <= 100; i++) {
                                Thread.sleep(100);
                                handler.sendEvent(503);
                            }
                        } catch (InterruptedException e) {
                            e.toString();
                        }
                    });

    private class TaskHandler extends EventHandler {
        int progress;
        int progress2;

        private TaskHandler(EventRunner runner) {
            super(runner);
        }

        @Override
        public void processEvent(InnerEvent event) {
            super.processEvent(event);
            if (event == null) {
                return;
            }
            int eventId = event.eventId;
            switch (eventId) {
                case 502:
                    progressBarDeterminate.setProgress(progress++);
                    break;
                case 503:
                    progressBarIndeterminateDeterminate.setProgress(progress2++);
                    break;
            }
        }
    }
}
