# Material Design Library

### Demo
![MaterialDesignLibrary](https://gitee.com/openharmony-tpc/MaterialDesignLibrary/raw/master/screenshot/demo.gif)

# Usage

Solution 1: local har package integration
1. Add the har package to the lib folder.
2. Add the following code to gradle of the entry:
```groovy
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```

Solution 2:
```groovy
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:MaterialDesignLibrary:1.0.1'
```

## Entry Running Requirements
1. Use DevEco Studio and download the SDK.
2. Change the dependencies→classpath version in the build.gradle file to the corresponding version. (that is, the version used in your IDE's new project)


## How to use

Some components have custom attributes, if you want use them, you must add this line in your xml file in the first component:

```xml
<DependentLayout xmlns:ohos="http://schemas.huawei.com/res/ohos"
                 ohos:width="match_parent"
                 ohos:height="match_parent">
</DependentLayout>
```

>If you are going to use a ScrollView, it is recommended that you use the CustomScrollView provided in this library to avoid problems with the custom components.
>To use this component:
```xml
<com.gc.materialdesign.views.ScrollView xmlns:ohos="http://schemas.huawei.com/res/ohos
    ohos:id="$+id:scroll"
    ohos:width="match_parent"
    ohos:height="match_parent">
</com.gc.materialdesign.views.ScrollView>
```

###### Flat Button

```xml
<com.gc.materialdesign.views.ButtonFlat
                ohos:id="$+id:buttonflat"
                ohos:width="match_content"
                ohos:height="match_content"
                materialdesign:background="#1E88E5"
                ohos:text="Button" />
```

###### Rectangle Button

```xml
<com.gc.materialdesign.views.ButtonRectangle
                ohos:id="$+id:button"
                ohos:width="match_content"
                ohos:height="match_content"
                materialdesign:background="#1E88E5"
                ohos:text="Button"/>
```

###### Float Button

>It is recommended to put this component in the right-bottom of the screen. To use this component write this code in your xml file.
>If you don`t want to start this component with animation set the animate attribute to false.
>Put your icon in the icon attribute to set the drawable icon for this component.

```xml
<DependentLayout xmlns:ohos="http://schemas.huawei.com/res/ohos"
                 xmlns:materialdesign="http://schemas.huawei.com/apk/res-auto"
                 ohos:width="match_parent"
                 ohos:height="match_parent"
    >
    <!-- ... XML CODE -->
    <com.gc.materialdesign.views.ButtonFloat
                ohos:id="$+id:buttonFloat"
                ohos:width="match_content"
                ohos:height="match_content"
                ohos:align_parent_bottom="true"
                ohos:align_parent_right="true"
                ohos:right_margin="24vp"
                materialdesign:animate="true"
                materialdesign:background="#1E88E5"
                materialdesign:iconDrawable="$media:ic_action_new"/>
</DependentLayout>
```

###### Float small button

```xml
<com.gc.materialdesign.views.ButtonFloatSmall
                ohos:id="$+id:buttonFloatSmall"
                ohos:width="match_content"
                ohos:height="match_content"
                materialdesign:background="#1E88E5"
                materialdesign:iconDrawable="$media:ic_action_new" />
```

###### CheckBox

```xml
<com.gc.materialdesign.views.CheckBox
                    ohos:id="$+id:checkBox"
                    ohos:width="match_content"
                    ohos:height="match_content"
                    ohos:alignment="center"
                    materialdesign:background="#1E88E5"
                    materialdesign:check="true"/>
```

###### Switch

```xml
<com.gc.materialdesign.views.Switch
                    ohos:id="$+id:switchView"
                    ohos:width="match_content"
                    ohos:height="match_content"
                    materialdesign:background="#1E88E5"
                    materialdesign:check="true"/>
```

###### Progress bar circular indeterminate

```xml
<com.gc.materialdesign.views.ProgressBarCircularIndeterminate
                ohos:id="$+id:progressBarCircularIndetermininate"
                ohos:width="32vp"
                ohos:height="32vp"
                materialdesign:background="#1E88E5"/>
```

###### Progress bar indeterminate

```xml
<com.gc.materialdesign.views.ProgressBarIndeterminate
                    ohos:id="$+id:progressBarIndeterminate"
                    ohos:width="match_parent"
                    ohos:height="match_content"
                    materialdesign:background="#1E88E5"/>
```

###### Progress bar indeterminate determinate

```xml
<com.gc.materialdesign.views.ProgressBarIndeterminateDeterminate
                ohos:id="$+id:progressBarIndeterminateDeterminate"
                ohos:width="match_parent"
                ohos:height="match_content"
                materialdesign:background="#1E88E5" />
```

>If you begin progrees, you only have to set progress it

```java
progressBarIndeterminateDeterminate.setProgress(progress);
```

###### Progress bar determinate

```xml
<com.gc.materialdesign.views.ProgressBarDeterminate
                ohos:id="$+id:progressDeterminate"
                ohos:width="match_parent"
                ohos:height="match_content"
                materialdesign:background="#1E88E5"/>
```

>You can custom max and min progress values with `materialdesign:max="50"` and `materialdesign:min="25"` attributes.

```xml
<com.gc.materialdesign.views.Slider
                ohos:id="$+id:slider"
                ohos:width="match_parent"
                ohos:height="match_content"
                materialdesign:background="#1E88E5"
                materialdesign:max="50"
                materialdesign:min="0"/>
```

## Widgets

```java
SnackBar snackbar = new SnackBar(Context context, String text, String buttonText, Component.ClickedListener onClickListener);
snackbar.show();
```

> If you don't want to show the button, put `null` in `buttonText` attribute

```java
Dialog dialog = new Dialog(Context context,String title, String message);
dialog.show();
```

>You can set the accept and cancel button on the event listener or change it's text
```java
// Set accept click listenner
dialog.setOnAcceptButtonClickListener(Component.ClickedListener onAcceptButtonClickListener);
// Set cancel click listenner
dialog.setOnCancelButtonClickListener(Component.ClickedListener onCancelButtonClickListener);
// Acces to accept button
ButtonFlat acceptButton = dialog.getButtonAccept();
// Acces to cancel button
ButtonFlat cancelButton = dialog.getButtonCancel();
>```

```java
ColorSelector colorSelector = new ColorSelector(Context context,String intialColor, OnColorSelectedListener onColorSelectedListener);
colorSelector.show();
```

Original link: https://github.com/navasmdc/MaterialDesignLibrary





